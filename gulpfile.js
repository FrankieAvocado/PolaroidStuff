var gulp = require("gulp");
var sass = require("gulp-sass");
var rename = require("gulp-rename");
var babel = require("gulp-babel");
var browserify = require("browserify");
var transform = require("vinyl-transform");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var del = require("del");

gulp.task("sass_compile", [], function(){
   return gulp.src("app/styles/main.scss")
       .pipe(sass())
       .pipe(gulp.dest("app/dist/styles"));
});

gulp.task("react_transform", [], function(){
   return gulp.src("app/sections/*.jsx")
       .pipe(babel({
           plugins: ["transform-react-jsx"]
       }))
       .pipe(gulp.dest("app/dist/transforms"));
});

gulp.task("browserify", ["react_transform"], function () {
    return browserify("app/dist/transforms/home.js").bundle()
        .pipe(source("app.js"))
        .pipe(buffer())
        .pipe(gulp.dest("app/dist"));
});

gulp.task("build", ["sass_compile", "react_transform", "browserify"]);