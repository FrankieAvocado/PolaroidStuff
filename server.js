var express = require("express");
var path = require("path");
var app = express();
var bodyParser = require("body-parser");
var MongoClient = require("mongodb").MongoClient;
var auth = require("./server/auth.js").authSystem;
var session = require("express-session");

//70277890-6543-470b-87a5-ff8eff850262
var dbpassword = process.env.DBPASS || "70277890-6543-470b-87a5-ff8eff850262";
var dbuser = process.env.DBPASS || "polaroid_user";
var dbUrl =  "mongodb://" + dbuser + ":" + dbpassword + "@ds041496.mlab.com:41496/heroku_vj1zcqj0";
var port = process.env.PORT || 3000;

var adminUserName = "test";
var adminPassword = "123";

app.use(bodyParser.json());
app.use(session({secret:"whatthefuckisthekeyforthisthingagain?"}));

app.get("/polaroids", function(req, res){
    var list = [];

    for(var i = 0; i < 10; i++){
        list.push({
            width: 550 + i,
            height: 600,
            alt: "Picture :" + i
        });
    }

    res.status(200).send(list).end();
});

app.get("/frames", function(req, res){

    MongoClient.connect(dbUrl, function(err, db){
        var frames = db.collection("frames");
        var result = frames.find().limit(100).toArray(function(err, result){
            if(err){
                res.status(400).send(err).end();
            }
            else{
                console.log(result);
                res.status(200).send(result).end();
            }
        });

    });

});


app.get("/*", function(request, response){
    if(request.path.indexOf("/app/") === 0){
        response.sendFile(__dirname + request.path);
    }
    else {
        response.sendFile(path.resolve(__dirname, "index.html"));
    }
});

var newAuth = auth(adminUserName, adminPassword);
app.post("/api/*", newAuth.requireAuthentication);
app.patch("/api/*", newAuth.requireAuthentication);
app.delete("/api/*", newAuth.requireAuthentication);

app.post("/api/frame", function(req, res){
    console.log(req.body);
    if(req.body.name){
        MongoClient.connect(dbUrl, function(err, db){
            var frames = db.collection("frames");
            frames.insertOne({
                name: req.body.name
            }, function(err, result){
                res.status(200).send(result.insertedId).end();
            });


        });
    }
    else{
        res.status(400).send("couldn't find body properties.").end();
    }
});

app.post("/api/photo", function(req, res){

    if(req.body.frameId){
        MongoClient.connect(dbUrl, function(err, db){
            var frames = db.collection("frames");
            frames.update(
                {"_id": ObjectID(req.body.frameId)},
                {$push: {"photos": req.body.data_uri}}
            );
            res.status(200).send().end();
        });
    }
    else{
        res.status(400).send("just couldn't insert that photo into the db, bro").end();
    }

});




app.post("/login", function(req, res){
    newAuth.userLogin(req, res);
});

app.get("/login/ask", function(req, res){
    var status = {"nope": !newAuth.isUserApproved(req)};
    res.status(200).send(status).end();
});

app.listen(port, function(){
	console.log("The audience is listening!");
});

