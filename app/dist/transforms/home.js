var React = require("react");
var ReactDOM = require("react-dom");

var browserHistory = require("react-router").browserHistory;
var Router = require("react-router").Router;
var Route = require("react-router").Route;
var Link = require("react-router").Link;

var Polaroid = require("./components.js").Polaroid;
var PictureFrame = require("./components.js").PictureFrame;
var FrameTitle = require("./components.js").FrameTitle;
var AJAX = require("../../awesome_sauce/ajax.js").AJAX;

var NewFrameForm = require("./forms.js").NewFrameForm;
var AddImageForm = require("./forms.js").AddImageForm;
var LoginForm = require("./forms.js").LoginForm;

var MainApp = React.createClass({
    displayName: "MainApp",
    render: function () {
        return React.createElement(
            "div",
            null,
            React.createElement(
                "section",
                { className: "TitleBar" },
                React.createElement(
                    "h1",
                    null,
                    "PolaroidRage"
                ),
                React.createElement(
                    Link,
                    { to: "/login" },
                    "Login"
                )
            ),
            this.props.children
        );
    }
});

var PolaroidsPage = React.createClass({
    displayName: "PolaroidsPage",
    getInitialState: function () {
        return {
            polaroids: []
        };
    },
    componentDidMount: function () {

        AJAX.get("/polaroids").then(result => {
            this.setState({
                polaroids: result
            });
        }, () => {
            this.setState({
                polaroids: []
            });
        });
    },
    componentWillUnmount: function () {
        if (this.serverRequest) {
            this.serverRequest.abort();
        }
    },
    render: function () {

        var internalPolaroids = this.state.polaroids.map(function (polaroid, i) {
            return React.createElement(Polaroid, { image: polaroid, key: i });
        });

        return React.createElement(
            "div",
            { className: "Content" },
            React.createElement(
                PictureFrame,
                null,
                React.createElement(
                    "section",
                    { className: "PictureHolder" },
                    internalPolaroids
                ),
                React.createElement(FrameTitle, { title: "Black & White", description: "This is a series of grayscale photos randomly loaded from the internet.  Notice how classy they look.  Clearly someone put time and thought into this.  But it wasn't me.  Because these photos are from the internet." })
            )
        );
    }
});

var LoginPage = React.createClass({
    displayName: "LoginPage",
    render: () => {
        return React.createElement(LoginForm, null);
    }
});

var AddFramePage = React.createClass({
    displayName: "AddFramePage",
    render: () => {
        return React.createElement(
            "div",
            null,
            React.createElement(NewFrameForm, null)
        );
    }
});

var UploadPage = React.createClass({
    displayName: "UploadPage",
    render: () => {
        return React.createElement(
            "div",
            null,
            React.createElement(AddImageForm, null)
        );
    }

});

var FrameListingPage = React.createClass({
    displayName: "FrameListingPage",
    getInitialState: function () {
        return {
            frames: []
        };
    },
    componentDidMount: function () {
        AJAX.get("/frames").then(result => {
            this.setState({
                frames: result
            });
        }, () => {
            this.setState({
                frames: []
            });
        });
    },
    componentWillUnmount: function () {
        if (this.serverRequest) {
            this.serverRequest.abort();
        }
    },
    render: function () {

        var internalFrames = this.state.frames.map(function (frame, i) {
            return React.createElement(
                "li",
                { key: i },
                React.createElement(
                    "a",
                    { href: "" },
                    frame.name
                )
            );
        });

        return React.createElement(
            "ul",
            null,
            internalFrames
        );
    }
});

ReactDOM.render(React.createElement(
    Router,
    { history: browserHistory },
    React.createElement(
        Route,
        { path: "/", component: MainApp },
        React.createElement(Route, { path: "/gallery", component: PolaroidsPage }),
        React.createElement(Route, { path: "/login", component: LoginPage }),
        React.createElement(Route, { path: "/addframe", component: AddFramePage }),
        React.createElement(Route, { path: "/framelisting", component: FrameListingPage }),
        React.createElement(Route, { path: "/upload", component: UploadPage })
    )
), document.getElementById("app"));