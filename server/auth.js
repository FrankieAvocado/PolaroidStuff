/**
 * Created by Edmund on 10/2/2016.
 */

var authSystem = function(user, password){

    var _user = user;
    var _password = password;

    var requireAuthentication = function(req, res, next){
        if(isUserApproved(req)){
          next();
        }
        else{
          res.status(404).end();
        }
    };

    var isUserApproved = function(req){
        return req.session.name === _user;
    };

    var userLogin = function(req, res){
        if(req.body.username === _user && req.body.password === _password){
            req.session.name = req.body.username;
            console.log("Wilkommen: " + req.session.name);
        }
        else{
            console.log("Get outta here: " + req.body.username);
            req.session.name = null;
        }

        res.status(200).end();
    };

    return{
        requireAuthentication: requireAuthentication,
        userLogin: userLogin,
        isUserApproved: isUserApproved
    };
};

module.exports = {
    authSystem: authSystem
};