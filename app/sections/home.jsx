var React = require("react");
var ReactDOM = require("react-dom");

var browserHistory = require("react-router").browserHistory;
var Router = require("react-router").Router;
var Route = require("react-router").Route;
var Link = require("react-router").Link;

var Polaroid = require("./components.js").Polaroid;
var PictureFrame = require("./components.js").PictureFrame;
var FrameTitle = require("./components.js").FrameTitle;
var AJAX = require("../../awesome_sauce/ajax.js").AJAX;

var NewFrameForm = require("./forms.js").NewFrameForm;
var AddImageForm = require("./forms.js").AddImageForm;
var LoginForm = require("./forms.js").LoginForm;

var MainApp = React.createClass({
    displayName: "MainApp",
    render: function(){
        return (
            <div>
                <section className="TitleBar">
                    <h1>PolaroidRage</h1>
                    <Link to="/login">Login</Link>
                </section>
                {this.props.children}
            </div>
        )
    }
});

var PolaroidsPage = React.createClass({
    displayName: "PolaroidsPage",
    getInitialState: function(){
      return{
          polaroids: []
      }
    },
    componentDidMount: function(){

        AJAX.get("/polaroids").then(
            (result) => {
                this.setState({
                  polaroids: result
                });
            },
            () => {
                this.setState({
                    polaroids: []
                });
            }
        );
    },
    componentWillUnmount: function(){
        if(this.serverRequest){
            this.serverRequest.abort();
        }
    },
    render: function(){

        var internalPolaroids = this.state.polaroids.map(function (polaroid, i) {
            return (
                <Polaroid image={polaroid} key={i}></Polaroid>
            )
        });

        return(
            <div className="Content">
                <PictureFrame>
                    <section className="PictureHolder">
                        {internalPolaroids}
                    </section>
                    <FrameTitle title="Black & White" description="This is a series of grayscale photos randomly loaded from the internet.  Notice how classy they look.  Clearly someone put time and thought into this.  But it wasn't me.  Because these photos are from the internet."></FrameTitle>
                </PictureFrame>
            </div>
        );
    }
});

var LoginPage = React.createClass({
    displayName: "LoginPage",
    render: () => {
        return (
            <LoginForm></LoginForm>
        )
    }
});


var AddFramePage = React.createClass({
    displayName: "AddFramePage",
    render: () => {
        return (
            <div>
                <NewFrameForm/>
            </div>
        )
    }
});

var UploadPage = React.createClass({
    displayName: "UploadPage",
    render: () => {
        return (
            <div>
                <AddImageForm></AddImageForm>
            </div>
        )
    }

});

var FrameListingPage = React.createClass({
    displayName: "FrameListingPage",
    getInitialState: function(){
        return {
            frames: []
        };
    },
    componentDidMount: function(){
        AJAX.get("/frames").then(
            (result) => {
                this.setState({
                    frames: result
                });
            },
            () => {
                this.setState({
                    frames: []
                });
            }
        )
    },
    componentWillUnmount: function(){
        if(this.serverRequest){
            this.serverRequest.abort();
        }
    },
    render: function(){

        var internalFrames = this.state.frames.map(function (frame, i) {
            return (
                <li key={i}><a href="">{frame.name}</a></li>
            )
        });

        return (
            <ul>
                {internalFrames}
            </ul>
        )
    }
});


ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={MainApp}>
            <Route path="/gallery" component={PolaroidsPage}/>
            <Route path="/login" component={LoginPage}/>
            <Route path="/addframe" component={AddFramePage}/>
            <Route path="/framelisting" component={FrameListingPage}/>
            <Route path="/upload" component={UploadPage}/>
        </Route>
    </Router>,
    document.getElementById("app")
);

