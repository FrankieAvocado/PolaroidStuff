var React = require("react");

module.exports = function(){

    var Polaroid = React.createClass({
        displayName: "Polaroid",
        render: function(){

            var backgroundUrl = this.props.image.url || "https://unsplash.it/g/" + this.props.image.width +"/" + this.props.image.height + "?random";
            var style={
              backgroundImage: "url(" + backgroundUrl + ")"
            };

            return(
                <div className="Polaroid" style={style} alt={this.props.image.alt} title={this.props.image.alt}></div>
            )
        }
    });

    var PictureFrame = React.createClass({
        displayName: "PictureFrame",
        render: function(){
            return <div className="PictureFrame">{this.props.children}</div>
        }
    });

    var FrameTitle = React.createClass({
        displayName: "FrameTitle",
        render: function(){
            return (<section className="Info">
                <h1>{this.props.title}</h1>
                <p>{this.props.description}</p>
            </section>)
        }
    });

    return {
        Polaroid: Polaroid,
        PictureFrame: PictureFrame,
        FrameTitle: FrameTitle
    };

}();