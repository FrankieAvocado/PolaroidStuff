var React = require("react");

module.exports = function () {

    var Polaroid = React.createClass({
        displayName: "Polaroid",
        render: function () {

            var backgroundUrl = this.props.image.url || "https://unsplash.it/g/" + this.props.image.width + "/" + this.props.image.height + "?random";
            var style = {
                backgroundImage: "url(" + backgroundUrl + ")"
            };

            return React.createElement("div", { className: "Polaroid", style: style, alt: this.props.image.alt, title: this.props.image.alt });
        }
    });

    var PictureFrame = React.createClass({
        displayName: "PictureFrame",
        render: function () {
            return React.createElement(
                "div",
                { className: "PictureFrame" },
                this.props.children
            );
        }
    });

    var FrameTitle = React.createClass({
        displayName: "FrameTitle",
        render: function () {
            return React.createElement(
                "section",
                { className: "Info" },
                React.createElement(
                    "h1",
                    null,
                    this.props.title
                ),
                React.createElement(
                    "p",
                    null,
                    this.props.description
                )
            );
        }
    });

    return {
        Polaroid: Polaroid,
        PictureFrame: PictureFrame,
        FrameTitle: FrameTitle
    };
}();