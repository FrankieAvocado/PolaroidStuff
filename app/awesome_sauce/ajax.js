/**
 * Created by Edmund on 9/25/2016.
 */
var $ = require("jQuery");

module.exports = function(){

    var getSomeJSON = function(location){
        var pinkieSwear = new Promise(function(resolve, reject){
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
                if(this.readyState === 4){
                    if(this.status === 200){
                        resolve(JSON.parse(this.responseText));
                    }else{
                        reject(this);
                    }

                }
            };

            xmlhttp.open("GET", location, true);
            xmlhttp.send();
        });

        return pinkieSwear;
    };

    var postSomeJSON = function(location, data){
        var pinkieSwear = new Promise(function(resolve, reject){

            $.ajax({
                type: "POST",
                contentType: 'application/json',
                url: location,
                data: JSON.stringify(data),
                success: function(result){resolve(result)},
                error: function(error){reject(error)}
            });

        });

        return pinkieSwear;
    };

    return {
        AJAX: {
            get: getSomeJSON,
            post: postSomeJSON
        }
    };

}();





