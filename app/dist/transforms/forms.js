var React = require("react");
var AJAX = require("../../awesome_sauce/ajax.js").AJAX;
var Router = require("react-router").Router;

module.exports = function () {

    var NewFrameForm = React.createClass({
        displayName: "NewFrameForm",
        handleSubmit: function (e) {
            e.preventDefault();
            AJAX.post("/api/frame", {
                "name": this.state.name
            }).then(result => {
                console.log(result);
            });
        },
        handleNameChange: function (e) {
            this.setState({
                name: e.target.value
            });
        },
        render: function () {
            return React.createElement(
                "form",
                { onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", placeholder: "Enter Name Here", onChange: this.handleNameChange }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Save"
                )
            );
        }
    });

    var LoginForm = React.createClass({
        displayName: "LoginForm",
        handleSubmit: function (e) {
            e.preventDefault();
            console.log(this.state);
            AJAX.post("/login", {
                username: this.state.username,
                password: this.state.password
            }).then(function (result) {
                var failedLogin = result.nope;

                if (failedLogin) {
                    alert("You failed to login, son!");
                } else {
                    var transitionTo = Router.transitionTo;
                    transitionTo("/addframe");
                }
            });
        },
        handleUsernameChange: function (e) {
            this.setState({
                username: e.target.value
            });
        },
        handlePasswordChange: function (e) {
            this.setState({
                password: e.target.value
            });
        },
        render: function () {
            return React.createElement(
                "form",
                { onSubmit: this.handleSubmit },
                React.createElement("input", { type: "text", placeholder: "User Name", onChange: this.handleUsernameChange }),
                React.createElement("input", { type: "password", onChange: this.handlePasswordChange }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Save"
                )
            );
        }
    });

    var AddImageForm = React.createClass({
        displayName: "AddImageForm",
        handleSubmit: function (e) {
            e.preventDefault();
            AJAX.post("/api/photo", {
                frameId: this.state.frameId,
                data_uri: this.state.data_uri
            });
        },
        onChange: function (e) {
            var reader = new FileReader();
            var file = e.target.files[0];
            reader.onload = upload => {
                this.setState({
                    data_uri: upload.target.result
                });
            };

            reader.readAsDataURL(file);
        },
        render: function () {
            return React.createElement(
                "form",
                null,
                React.createElement("input", { type: "file", onChange: this.onChange }),
                React.createElement("img", { style: "width: 550; height: 600;", src: this.state && this.state.data_uri ? this.state.data_uri : "" }),
                React.createElement(
                    "button",
                    { type: "submit" },
                    "Add"
                )
            );
        }
    });

    return {
        NewFrameForm: NewFrameForm,
        AddImageForm: AddImageForm,
        LoginForm: LoginForm
    };
}();